[![Docker Repository on Quay](https://quay.io/repository/pmav99/grass_docker_src/status "Docker Repository on Quay")](https://quay.io/repository/pmav99/grass_docker_src)

# Description

This repo contains Dockerfiles for compiling GRASS GIS from source.

This repo contains several branches. Each branch corresponds to a different docker image. There
are currently the following images:

* `base` is the image on which the subsequent images are based upon.  This image installs all the
  GRASS dependencies from the ubuntu repositories + builds and installs `proj`, `geos` and `gdal`
  from source.
* `trunk70` which builds `GRASS 7.0.#` from the SVN repo (i.e. the latest commit from
  `release_branch70`).
* `trunk71` which builds `GRASS 7.1.#` from the SVN repo (i.e. the latest commit from `trunk`).

Apart from those, there is also the following image:

* `stable7` installs the last stable release of GRASS 7 (e.g. 7.0.3 etc) from the `ubuntu-unstable`
  ppa.

If you don't know which image to use, use `stable7`.

# Usage

You should be able to pull each image directly from http://quay.io but you can also tweak the
Dockerfiles and build the images yourself.

## GRASS 7.0.2

This is the latest stable release.

```
docker pull quay.io/pmav99/grass_docker_src:702
```

## GRASS 7 latest (trunk)

```
docker pull quay.io/pmav99/grass_docker_src:trunk7
```


Just execute `build.sh` and you will end up in a root console.  There is also a normal user named
`grassuser` who has passwordless suod enabled and also has some sample GRASS locations in his home
directory.

## GUI

In order to be able to run the GUI you need to enable docker to access the host's X server:
```
xhost +local:docker
```

